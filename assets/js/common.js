$(function() {
	/*tabs*/
	//  $(".js-tab-item").not(":first-child").hide();
	//  $(".js-tab").click(function() {
	// 		 if(!$(this).hasClass('active')){
	// 				 var tabsContainer = $(this).closest(".js-tabs"),
	// 						 tabs = tabsContainer.find(".js-tab"),
	// 						 tabsItems = tabsContainer.find(".js-tab-item");                
	// 				 tabs.removeClass("active").eq($(this).index()).addClass("active");
	// 				 tabsItems.hide().eq($(this).index()).fadeIn()
	// 		 }
	//  }).eq(0).addClass("active");
	$('[data-fancybox="images"]').fancybox({
		arrows: true, //Отображает кнопки навигации по краям экрана
		infobar: false, //При необхходимости - отключает инфорбар вверху (стрелки и счетчик)
		toolbar: true, //При необходимости - включает панель инструментов в правом верхнем углу
		buttons: [
			// "zoom",
			//"share",
			// "slideShow",
			//"fullScreen",
			//"download",
			// "thumbs",
			"close"
		]
	});
	$('.mmenu-top .close').on('click', function (e) {
		e.preventDefault();
		$('.mmenu').addClass('collapsed');
	});
	$(document).mouseup(function (e) {
    var elem = $(".mmenu");
    if(e.target!=elem[0]&&!elem.has(e.target).length){
			$('.mmenu').addClass('collapsed');
    }
});
//	open main menu (mobile mode)
$('.header .bar').on('click', function (e) {
	e.preventDefault();
	$('.mmenu').removeClass('collapsed');
});
	var menu = ['Cyclo-(rgdfk)', 'E[cyclo(rgdfk)]2', 'cyclo[rgdfk(tpp)]']
	var mySwiper = new Swiper ('.swiper-container', {
		direction: 'vertical',
			// If we need pagination
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
					renderBullet: function (index, className) {
						return '<span class="' + className + '">' + (menu[index]) + '</span>';
					},
			},
	
			// Navigation arrows
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})

	$(".open-popup-search").magnificPopup({
		type: "inline",
		midClick: true,
		showCloseBtn: false
	});
});