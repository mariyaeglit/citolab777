let browserSync = require('browser-sync'),
	gulp = require('gulp'),
	del = require('del'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	gutil = require('gulp-util'),
	ftp = require('vinyl-ftp'),
	notify = require('gulp-notify');

gulp.task('common-js', function () {
	return gulp.src([
		'assets/js/common.js',
	])
		.pipe(gulp.dest('assets/js'));
});

gulp.task('scripts', function () {
	return gulp.src([
		'assets/libs/jquery/dist/jquery.min.js',
		'assets/libs/swiper/swiper.min.js',
		'assets/libs/magnific-popup/jquery.magnific-popup.min.js',
		'assets/libs/jquery/jquery.fancybox.min.js',
		// 'assets/libs/owl/owl.carousel.min.js',
		// 'assets/libs/aos/aos.js',
	])
		.pipe(concat('scripts.min.js'))
		// .pipe(uglify()) // Минимизировать весь js (на выбор)
		.pipe(gulp.dest('assets/js'))
		.pipe(browserSync.reload({ stream: true }));
});

gulp.task('browserSync', function () {
	browserSync({
		server: {
			baseDir: 'assets'
		},
		notify: false,
		// online: false, // Work offline without internet connection
		// tunnel: true, tunnel: 'projectname', // Demonstration page: http://projectname.localtunnel.me
	})
});

gulp.task('sass', function () {
	return gulp.src('assets/sass/**/*.sass')
		.pipe(sass().on("error", notify.onError()))
		.pipe(rename({ suffix: '.min', prefix: '' }))
		.pipe(autoprefixer(['last 15 versions']))
		.pipe(cleanCSS())
		.pipe(gulp.dest('assets/css'))
		.pipe(browserSync.reload({ stream: true }));
});


gulp.task('deploy', function() {
	var conn = ftp.create({
	host:      'bukino.ru',
	user:      'web_bukinoru',
	password:  '9N3l6Y0o',
	parallel: 10,
	maxConnections: 1,
	//reload: true,
	log: gutil.log
});

var globs = [
	'dist/**',
	];
	return gulp.src(globs, {buffer: false})
	//.pipe(conn.newer('/www/bukino.ru/html/royaltaft/'))
	.pipe(conn.dest('/www/bukino.ru/html/royaltaft/'));
});

gulp.task('watch', ['sass', 'browserSync'], function () {
	gulp.watch('assets/sass/**/*.sass', ['sass']);
	gulp.watch(['libs/**/*.js', 'assets/js/common.js'], ['scripts']);
	gulp.watch('assets/*.html', browserSync.reload);
});

gulp.task('imagemin', function () {
	return gulp.src('assets/img/**/*')
		.pipe(cache(imagemin()))
		.pipe(gulp.dest('dist/img'));
});

gulp.task('build', ['removedist', 'imagemin', 'sass', 'scripts'], function() {

	var buildFiles = gulp.src([
		'assets/*.html',
		'assets/.htaccess',
	]).pipe(gulp.dest('dist'));

	var buildCss = gulp.src([
		'assets/css/style.min.css',
		'assets/css/normalize.css',
	]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'assets/js/common.js',
	]).pipe(gulp.dest('dist/js'));

	var buildJs = gulp.src([
		'assets/js/scripts.min.js',
	]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'assets/css/fonts/**/*',
	]).pipe(gulp.dest('dist/css/fonts'));

});

gulp.task('removedist', function () { return del.sync('dist'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);